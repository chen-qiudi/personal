import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		// 获取各个项目的最高分
		Map<String, Double> totalScore = getTotalScore("total.properties");
		
//		System.out.println(totalScore);
		
		// 生成个人成绩
		// 获取要解析的两个html
		File fileNameSmall=new File("src/small.html");
		File fileNameAll=new File("src/all.html");
		Map<String, Double> mScore = initmScore(fileNameSmall, fileNameAll);

		// 计算个人最终期末得分
		System.out.println(String.format("%.2f", getFinalScore(totalScore, mScore)));

	}

	public static double getFinalScore(Map<String, Double> totalScore, Map<String, Double> mScore) {
		
		
		
		// 最终成绩的before得分=before所得分数/应得分数
		double before = mScore.get("before") / totalScore.get("before") * 100;
		// 最终成绩的base得分=base所得分数/应得分数*0.95
		double base = mScore.get("base") / totalScore.get("base") * 100 * 0.95;
		// 最终成绩的test得分=test所得分数/应得分数
		double test = mScore.get("test") / totalScore.get("test") * 100;
		// 最终成绩的编程题得分=program得分/最高分
		double program = mScore.get("program") / totalScore.get("program") * 100;
		if (program > 95) {
			program = 95;
		}
		// 最终成绩的附加题得分=add得分/最高分
		double add = mScore.get("add") / totalScore.get("add") * 100;
		if (add > 90) {
			add = 90;
		}
		// 计算个人最终期末得分
		double finalScore = before * 0.25 + base * 0.3 + test * 0.2 + program * 0.1 + add * 0.05 + 10;
		return finalScore;
	}

	public static Map<String, Double> initmScore(File fileNameSmall, File fileNameAll) throws IOException {
		Map<String, Double> mScore = new HashMap<String, Double>();
		mScore.put("before", .0);
		mScore.put("base", .0);
		mScore.put("test", .0);
		mScore.put("program", .0);
		mScore.put("add", .0);

		// 获取要解析的两个html
		Document small=Jsoup.parse(fileNameSmall,"utf-8");
		Document all=Jsoup.parse(fileNameAll,"utf-8");
		
		
		
		// 获取所有需要提取经验值的节点
		Elements activities = small.getElementsByClass("interaction-row");
		// 在small文件节点数组后面追加all文件里要提取的节点
		activities.addAll(all.getElementsByClass("interaction-row"));

		for (int i = 0; i < activities.size(); i++) {
			// 判断活动类别
			if (activities.get(i).child(1).child(0).toString().indexOf("课堂完成") != -1) {
				// 提取base值
				if (activities.get(i).child(1).child(2).toString().indexOf("已参与&nbsp;") != -1) {
					Scanner reader = new Scanner(
							activities.get(i).child(1).child(2).children().get(0).children().get(7).text());
					mScore.put("base", mScore.get("base") + reader.nextDouble());
				} // 判断活动类别
			} else if (activities.get(i).child(1).child(0).toString().indexOf("课堂小测") != -1) {
				// 提取test值
				if (activities.get(i).child(1).child(2).toString().indexOf("已参与&nbsp;") != -1) {
					Scanner reader = new Scanner(
							activities.get(i).child(1).child(2).children().get(0).children().get(7).text());
					mScore.put("test", mScore.get("test") + reader.nextDouble());
					if (activities.get(i).child(1).child(2).children().get(0).children().size() >= 9) {
						reader = new Scanner(
								activities.get(i).child(1).child(2).children().get(0).children().get(9).text());
						reader.next();
						mScore.put("test", mScore.get("test") + reader.nextDouble());
					}
				} // 判断活动类别
			} else if (activities.get(i).child(1).child(0).toString().indexOf("编程题") != -1) {
				// 提取program值
				if (activities.get(i).child(1).child(2).toString().indexOf("已参与&nbsp;") != -1) {
					Scanner reader = new Scanner(
							activities.get(i).child(1).child(2).children().get(0).children().get(7).text());
					mScore.put("program", mScore.get("program") + reader.nextDouble());
				} // 判断活动类别
			} else if (activities.get(i).child(1).child(0).toString().indexOf("附加题") != -1) {
				// 提取add值
				if (activities.get(i).child(1).child(2).toString().indexOf("已参与&nbsp;") != -1) {
					Scanner reader = new Scanner(
							activities.get(i).child(1).child(2).children().get(0).children().get(7).text());
					mScore.put("add", mScore.get("add") + reader.nextDouble());
				} // 判断活动类别
			} else if (activities.get(i).child(1).child(0).toString().indexOf("课前自测") != -1) {
				// 提取before值
				if (activities.get(i).child(1).child(2).toString().indexOf("color:#8FC31F;") != -1) {
					Scanner reader = new Scanner(
							activities.get(i).child(1).child(2).children().get(0).children().get(10).text());
					mScore.put("before", mScore.get("before") + reader.nextDouble());

				}
			}
		}
		return mScore;
	}

	public static Map<String, Double> getTotalScore(String fileName) {
		Properties n = new Properties();
		Map<String, Double> totalScore = new HashMap<String, Double>();
		try {
			n.load(new FileInputStream(fileName));
			totalScore.put("before", Double.parseDouble(n.getProperty("before")));
			totalScore.put("base", Double.parseDouble(n.getProperty("base")));
			totalScore.put("test", Double.parseDouble(n.getProperty("test")));
			totalScore.put("program", Double.parseDouble(n.getProperty("program")));
			totalScore.put("add", Double.parseDouble(n.getProperty("add")));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return totalScore;
	}
}
